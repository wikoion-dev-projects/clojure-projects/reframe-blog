FROM registry.gitlab.com/container-images1/shadow-cljs-docker-image:latest 

WORKDIR reframe-blog

COPY ./ ./

RUN npm install
RUN npx shadow-cljs release app

EXPOSE 3000

CMD ["npx", "shadow-cljs", "server"]