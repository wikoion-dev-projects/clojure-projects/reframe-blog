const cacheName = 'cache-v1';
const precacheResources = [
  '/',
  '/index.html',
  '/js/main.js',
  '/css/styles.css',
  '/fonts/Ailerons/Ailerons-Regular.otf',
  '/img/ansible.jpg',
  '/img/ansible.png',
  '/img/arch.jpg',
  '/img/dotfiles.png',
  '/img/draycott-live-blog.gif',
  '/img/favicon.ico',
  '/img/github-icon.png',
  '/img/gitlab-icon.svg',
  '/img/go.png',
  '/img/reframe.png',
  '/img/traefik.png',
  '/img/twitter-icon.svg'
];

self.addEventListener('install', event => {
  console.log('Service worker install event!');
  event.waitUntil(
    caches.open(cacheName)
      .then(cache => {
        cache.add('https://gitlab.com/wikoion-dev-projects/draycott-live-blog/-/raw/master/src/main/resources/static/images/home-bg.jp')
        return cache.addAll(precacheResources);
      })
  );
});

self.addEventListener('activate', event => {
  console.log('Service worker activate event!');
});

self.addEventListener('fetch', event => {
  console.log('Fetch intercepted for:', event.request.url);
  event.respondWith(caches.match(event.request)
    .then(cachedResponse => {
        if (cachedResponse) {
          return cachedResponse;
        }
        return fetch(event.request);
      })
    );
});
