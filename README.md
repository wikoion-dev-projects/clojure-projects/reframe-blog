# PWA Blog application created to learn how to use Clojurescript, Reagent and Re-frame
### *Still a work in progress*
### Installation
You will need npm, node and shadowcljs installed
#### Using Docker
```
git clone https://gitlab.com/wikoion-dev-projects/clojure-projects/reframe-blog.git
cd reframe-blog
docker build -t reframe-blog:testing .
docker run -d -p 3000:3000 reframe-blog:testing
```

#### Manually
#### *Deps: npm, shadow-cljs, node*
```
git clone https://gitlab.com/wikoion-dev-projects/clojure-projects/reframe-blog.git
cd reframe-blog
npm install
npx shadow-cljs server
npx shadow-cljs release app

```
