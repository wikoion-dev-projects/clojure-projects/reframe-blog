(ns reframe-blog.views
  (:require
   [reagent.core :as r]
   [re-frame.core :as rf]
   [reframe-blog.subs :as subs]
   [reframe-blog.events :as events]
   [reframe-blog.post.subs :as post-subs]
   [reframe-blog.nav.subs :as nav-subs]
   [reframe-blog.nav.views.nav :refer [toolbar]]
   [reframe-blog.footer.views.footer :refer [footer]]
   [reframe-blog.post.views.posts :refer [posts-page]]
   [reframe-blog.post.views.post :refer [post-page]]
   [reframe-blog.about.views.about :refer [about-page]]
   [reframe-blog.home.views.home :refer [home-page]]))

;; home
(defn home-panel []
  [:<>
   [home-page]])

;; about
(defn about-panel []
  [:<>
   [about-page]])

;; post
(defn post-panel []
  [:<>
   (rf/dispatch [::events/get-posts])
   [:div
    (let [post @(rf/subscribe [::post-subs/post])]
      [post-page post])]])

;; posts
(defn posts-panel []
  [:<>
   [posts-page]])

;; main
(defn- panels [panel-name]
  (case panel-name
    :home [home-panel]
    :about [about-panel]
    :post [post-panel]
    :posts [posts-panel]
    :search [posts-panel]
    [home-panel]))

(defn show-panel [panel-name]
  [panels panel-name])

(defn main-panel []
  [:<>
   [toolbar]
   (let [active-panel (rf/subscribe [::nav-subs/active-page])]
     [show-panel @active-panel])
   [footer]])
