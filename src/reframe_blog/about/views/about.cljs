(ns reframe-blog.about.views.about
  (:require
   ["@material-ui/core" :refer [Box Link Icon Button Grid]]
   [reframe-blog.about.subs :as subs]
   [reframe-blog.footer.views.footer :refer [footer]]
   [re-frame.core :as rf]
   [reagent.core :as r]))

(defn project-card []
  (let [projects (rf/subscribe [::subs/highlighted-projects])]
    (for [project @projects]
      ^{:key project}
      [(r/adapt-react-class Grid)
       {:item true}

       [(r/adapt-react-class Link)
        {:href (:href project)
         :class "card"
         :id "project-card"}

        [(r/adapt-react-class Box)
         {:bgcolor "#1b262f"
          :box-shadow 2
          :class "card"
          :id "project-card"
          :href (:href project)}

         [:img.card
          {:src (:image project)}]

         [:h1.card (:name project)]

         [:p.card
          (:description project)]]]])))

(defn about-page []
  [:> Box
   {:class "about"}
   [:h1 "About"]
   [:h2 "Infrastructure & Devops Specialist"]
   [:h2 "Skills"]
   [:ul.about
    [:li "Language experience: Java, Kotlin, Clojure, Ruby, Go, Python, JS, etc."]
    [:li "Framework experience: Spring, Rails, Luminus, React, Redux, Reagent, Re-Frame, etc."] 
    [:li "Docker/Kubernetes development and deployments in private and hybrid cloud environments"]  
    [:li "Provisioning tools: Ansible, Puppet, Chef, Spire, Teraform, etc."]
    [:li "AWS, Azure and Digital Ocean cloud platforms"]
    [:li "Virtualisation platforms: KVM, ESXI, Hyper-V, Xen, V-Sphere etc."]
    [:li "Networking, firewall/load-balancer technologies include: ZXTM, CISCO, Watchguard, Sophos, Aruba"]]

   [:h3.about
    "Highlighted Projects"]

   [:> Grid
    {:container true
     :direction "row"
     :spacing 10
     :justify "center"}
    (project-card)]])
