(ns reframe-blog.about.subs
  (:require
   [re-frame.core :as rf]))

(rf/reg-sub
 ::highlighted-projects
 (fn [db _]
   (:highlighted-projects db)))
