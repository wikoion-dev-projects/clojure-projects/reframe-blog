(ns reframe-blog.post.subs
  (:require
   [re-frame.core :as rf]))

(rf/reg-sub
 ::search-results
 (fn [db _]
   (:search-results db)))

(rf/reg-sub
 ::posts
 (fn [db _]
   (:posts db)))

(rf/reg-sub
 ::post
 (fn [db _]
   (let [active-post (get-in db [:nav :active-post])
         posts (:posts db)]
     (into {} (filter #(= (:id %) active-post) posts)))))

(rf/reg-sub
 ::active-post
 (fn [db _]
   (get-in db [:nav :active-post])))
