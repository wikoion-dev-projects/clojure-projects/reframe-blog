(ns reframe-blog.post.views.post-card
  (:require
   ["@material-ui/core" :refer [Box Button Card Grid Link]]
   [reframe-blog.router :as router]
   [reframe-blog.post.subs :as subs]
   [reframe-blog.post.events :as events]
   [reframe-blog.nav.events :as nav-events]
   [re-frame.core :as rf]
   [reagent.core :as r]))

(defn post-cards [posts]
  [:> Grid
   {:container true
    :class "posts-grid"
    :direction "row"
    :spacing 10
    :justify "center"}
   
   (for [post @posts]
     ^{:key post}
     [(r/adapt-react-class Grid)
      {:item true}
      
      [(r/adapt-react-class Link)
       {:href (router/path-for :post :post-id (:id post))
        :on-click #(rf/dispatch [::nav-events/set-active-page :post :post-id (:id post)])
        :class "card"
        :id "post-card"}

       [(r/adapt-react-class Box)
        {:bgcolor "#1b262f"
         :box-shadow 2
         :class "card"
         :id "card"
         :href (router/path-for :post :post-id (:id post))}
       
        [:img {:src (:image post)
               :class "card"
               :style {:width "100%"
                       :object-fit "cover"
                       :position "relative"}}]
        [:> Box
         {:class "title-post"}
         [:h1.card
          (let [title (apply str (take 40 (:title post)))]
            (if (> (count (:title post)) 40)
              (str title "...")
              title))]]
        
        [:p.card
         (str (apply str (take 50 (:body post))) "...")]]]])])
