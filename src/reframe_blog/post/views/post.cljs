(ns reframe-blog.post.views.post
  (:require
   ["@material-ui/core" :refer [Box Button Drawer Grid]]
   ["moment" :as moment]
   [reagent.core :as r]
   [markdown-to-hiccup.core :as m]))

(defn post-image [post]
  (let [image (:image post)
        body (:body post)
        title (:title post)
        date (moment (js/parseInt (:date post)))]

      [:> Box
       [:> Box
        {:id "post-image"
         :box-shadow 2
         :style {:width "100%"
                 :height "600px"
                 :overflow "hidden"
                 :background-image (str "linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.75) ), url(" image ")")
                 :background-size "cover"
                 :display "flex"
                 :justify-content "center"
                 :align-items "center"
                 }}

        [:> Box
         {:id "post"
          :text-align "center"}
         [:h1.post
          title]
         [:h3.post
          (. date format "DD-MM-YY")]]]
       
       [:> Box
        {:text-align "center"
         :style {:margin-top 50}}
        (->> body
         (m/md->hiccup)
         (m/component))]]))

(defn post-page [post]
  (let [body (:body post)]
  (post-image post)))

