(ns reframe-blog.post.views.posts
  (:require
   [re-frame.core :as rf]
   [reagent.core :as r]
   [reframe-blog.post.subs :as subs]
   [reframe-blog.subs :as core-subs]
   [reframe-blog.nav.subs :as nav-subs]
   [reframe-blog.events :as events]
   [reframe-blog.post.views.post-card :refer [post-cards]]))

(defn posts-page []
  (let [all-posts (rf/subscribe [::subs/posts])
        search-results (rf/subscribe [::subs/search-results])]
    (if (= @(rf/subscribe [::nav-subs/active-page]) :posts)
      [post-cards all-posts]
      [post-cards search-results])))
