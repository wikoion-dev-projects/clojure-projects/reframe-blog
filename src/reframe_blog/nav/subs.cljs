(ns reframe-blog.nav.subs
  (:require
   [re-frame.core :as rf]))

(rf/reg-sub
 ::active-nav
 (fn [db _]
   (get-in db [:nav :active-nav])))

(rf/reg-sub
 ::active-page
 (fn [db _]
   (get-in db [:nav :active-page])))

(rf/reg-sub
 ::visibility
 (fn [db _]
   (:visibility db)))

(rf/reg-sub
 ::search-popover-visibility
 (fn [db _]
   (:search-popover-visibility db)))

(rf/reg-sub
 ::search-popover-anchor
 (fn [db _]
   (:search-popover-anchor db)))

(rf/reg-sub
 ::window-size
 (fn [db _]
   (:window-size db)))

(rf/reg-sub
 ::nav-search-value
 (fn [db _]
   (:nav-search-value db)))

(rf/reg-sub
 ::search-results
 (fn [db _]
   (:search-results db)))
