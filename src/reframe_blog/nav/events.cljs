(ns reframe-blog.nav.events
  (:require
   [re-frame.core :as rf]
   [reframe-blog.router :as router]
   [reframe-blog.events :as events]
   [reframe-blog.dev :refer [api-url]]
   [ajax.core :as ajax]
   [day8.re-frame.http-fx]))

(def nav-interceptors [(rf/path :nav)])

(rf/reg-fx
 ::navigate-to
 (fn [{:keys [path]}]
   (router/set-token! path)))

(rf/reg-event-fx                             
  ::search-posts                      
  (fn [{:keys [db]} [_ query]]                    
    {:db   (assoc-in db [:search-loading?] true)   
     :http-xhrio {:method          :get
                  :uri             (str (api-url) "?MatchAll=" query)
                  :timeout         8000                                           
                  :format          (ajax/json-request-format)
                  ;; :headers         {"Authorization"
                  ;;                   (str "bearer " "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VybmFtZSI6ImFkbWluIn0.X5qKoep6z071prwVB4EKUhGvNac18M9SZvpLKgVQaiY")}
                  :response-format (ajax/json-response-format {:keywords? true})  
                  :on-success      [::good-search-result]
                  :on-failure      [::bad-search-result]}}))

(rf/reg-event-db
 ::good-search-result
 (fn [db [_ result]]
   (-> db
       (assoc-in [:search-loading?] false)
       (assoc-in [:search-results]
                 (-> result
                     (events/lowercase-keywordise)
                     (events/keywordise-id)
                     (events/upper-case-title))))))

(rf/reg-event-db
 ::bad-search-result
 (fn [db [_ result]]
   (-> db
       (assoc-in [:failure-search-result] result)
       (assoc-in [:search-results] [{}])
       (assoc-in [:search-loading?] false))))

(rf/reg-event-fx
 :route-changed
 nav-interceptors
 (fn [{nav :db} [_ {:keys [handler route-params]}]]
   (let [nav (assoc nav :active-page handler)]
     (case handler
       :home
       {:db nav
        :dispatch [::events/get-posts]}

       :posts
       {:db nav
        :dispatch [::events/get-posts]}
        
       :search
       {:db nav
        :dispatch [::search-posts (:search-query route-params)]}

       :post
       {:db (assoc nav :active-post (keyword (:post-id route-params)))
        :dispatch [::events/get-posts]}

       {:db (dissoc nav :active-post)}))))

(rf/reg-event-db
 ::set-active-nav
 (fn [db [_ active-nav]]
   (assoc-in db [:nav :active-nav] active-nav)))

(rf/reg-event-db
 ::set-active-page
 (fn [db [_ active-page]]
   (assoc-in db [:nav :active-page] active-page)))

(rf/reg-event-db
 ::set-visibility
 (fn [db [_ visibility]]
   (assoc-in db [:visibility] visibility)))

(rf/reg-event-db
 ::set-search-popover-visibility
 (fn [db [_ visibility]]
   (assoc-in db [:search-popover-visibility] visibility)))

(rf/reg-event-db
 ::set-search-popover-anchor
 (fn [db [_ anchor-el]]
   (assoc-in db [:search-popover-anchor] anchor-el)))

(rf/reg-event-db
 ::set-window-size
 (fn [db [_ window-size]]
   (assoc-in db [:window-size] window-size)))

(rf/reg-event-fx
 ::set-nav-search-value
 (fn [{:keys [db]} [_ value]]
   {:db (assoc-in db [:nav-search-value] value)
    :dispatch [::search-posts value]}))
