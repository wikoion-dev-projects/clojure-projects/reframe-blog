(ns reframe-blog.nav.views.authenticated
  (:require
   [reagent.core :as r]
   [re-frame.core :as rf]
   [reframe-blog.router :as router]
   [reframe-blog.nav.subs :as subs]
   [reframe-blog.nav.events :as events]
   [reframe-blog.nav.views.nav-item :refer [nav-item]]
   ["@material-ui/core" :refer [Box Button Drawer IconButton]]
   ["@material-ui/icons/Menu" :refer [ListIcon]]
   ["@material-ui/system" :refer [shadows]]
   ))

(defn authenticated []
  (let [active-page @(rf/subscribe [::subs/active-page])
        nav-items
        [{:id :create
          :name "Create"
          :href (router/path-for :create)
          :dispatch #(rf/dispatch [::events/set-active-page :create])
          :active-page active-page}]]

    [:> Box {:display "flex"
             :justify-content "flex-end"
             :class "nav-authenticated"}

     (for [item nav-items]
       ^{:key item}
       [nav-item item])]))
