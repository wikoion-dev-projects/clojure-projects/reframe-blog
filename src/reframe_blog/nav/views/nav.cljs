(ns reframe-blog.nav.views.nav
  (:require
   [reagent.core :as r]
   [re-frame.core :as rf]
   ["@material-ui/core" :refer [Box Button Drawer IconButton ClickAwayListener Popover TextField]]
   ["@material-ui/icons" :refer [Menu MenuOpen]]
   ["@material-ui/system" :refer [shadows]]
   [reframe-blog.router :as router]
   [reframe-blog.nav.events :as events]
   [reframe-blog.events :as core-events]
   [reframe-blog.nav.subs :as subs]
   [reframe-blog.nav.views.public :refer [public]]
   [reframe-blog.nav.views.nav-item :refer [nav-item]]
   [reframe-blog.components.nav-search-field :refer [search-field search-component auto-complete]]
   [reframe-blog.nav.views.authenticated :refer [authenticated]]))

(def drawer-width 150)
(def body-margin 30)

(defn event-value
  [^js/Event e]
  (let [^js/HTMLInputElement el (.-target e)]
    (.-value el)))

(def input-component
  (r/reactify-component
   (fn [props]
     [:input (-> props
                 (assoc :ref (:inputRef props))
                 (dissoc :inputRef))])))

(defn auto-complete-container []
  (let [search-query @(rf/subscribe [::subs/nav-search-value])
        dispatch #(rf/dispatch [::events/set-active-page :search :search-query search-query])]
   [:> Box
    {:class "search-container"
     :display "flex"
     :style {:width "300px"}}

    [:form
     {:on-submit
      (fn [e]
        (.preventDefault e)
        (router/set-token! (router/path-for :search :search-query search-query))
        (rf/dispatch [::events/set-active-page :search])
        (rf/dispatch [::events/set-search-popover-visibility false])
        (rf/dispatch [::events/set-visibility false]))
      :style {:width "100%"}}
     [search-component]]]))

(defn shrink [default-margin]
  (let [drawer-visibility @(rf/subscribe [::subs/visibility])]
  (if drawer-visibility
    (str "calc(" (+ default-margin drawer-width) "px + 20vw)")
    default-margin)))

(defn drawer [drawer-local-width]
  (let [drawer-visibility @(rf/subscribe [::subs/visibility])]
    [:> ClickAwayListener
     {:on-click-away #(if (= true drawer-visibility)
                        (rf/dispatch [::events/set-visibility false]))}
     [:> Drawer
      {:anchor "left"
       :class "drawer"
       :open drawer-visibility
       :variant "persistent"}

      [:> Box
       {:width (str "calc(" drawer-local-width "px + 20vw)")
        :class "drawer-container"
        :style {:padding-left "10px"
                :padding-right "10px"}}

       [auto-complete-container]
       [public {:display "flex"
                :justify-content "center"
                :class "nav-public"
                :style {:display "block"}}]]]]))

(defn drawer-button []
  (let [drawer-visibility @(rf/subscribe [::subs/visibility])]
    [:> Button
     {:size "small"
      :aria-label "menu"
      :class "nav-icon"
      :diabled "false"
      :on-click #(rf/dispatch [::events/set-visibility (not drawer-visibility)])}
     (if drawer-visibility
       [:> MenuOpen]
       [:> Menu])]))

(defn drawer-handler [f element]
  (r/with-let [window-size (rf/subscribe [::subs/window-size])
               handler #(rf/dispatch [::events/set-window-size (.-innerWidth js/window)])
               _ (.addEventListener js/window "resize" handler)]
    (rf/dispatch [::events/set-search-popover-visibility false])
    (if (f @window-size 900)
      [element])
    (finally (.removeEventListener js/window "resize" handler))))

(defn nav []
    [:> Box
     {:display "flex"
      :bgcolor "#161d25"
      :class "nav-container"}

     [drawer-handler < drawer-button]
     [drawer drawer-width]

     [nav-item {:id :home
                :name "CALL ME JOHNSON"
                :href (router/path-for :home)
                :dispatch #(rf/dispatch [::events/set-active-nav :home])
                :active-page @(rf/subscribe [::subs/active-page])}]
      
     [:> Box
      {:display "flex"
       :justify-content "flex-end"
       :class "nav-public"}

      [drawer-handler > auto-complete-container]

      (let [public-nav #(public {:display "flex"
                                 :class "nav-public"})]
        [drawer-handler > public-nav])]])

(defn toolbar []
  [:<>
   [:header
    {:class "nav-toolbar"
     :style {:margin-left (shrink 10)}}
    [nav]]])
