(ns reframe-blog.nav.views.nav-item
  (:require
   ["@material-ui/core" :refer [Box Button]]
   ))

(defn nav-item [{:keys [id name href dispatch active-page]}]
  [:> Box
   {:border-bottom (when (= active-page id) "2px solid #88c0d0")}
   [:> Button
    {:key id
     :id id
     :href href
     :on-click dispatch
     :pb 10
     :size "small"
     :class "nav-button"}
    name]])
