(ns reframe-blog.nav.views.public
  (:require
   [reagent.core :as r]
   [re-frame.core :as rf]
   ["@material-ui/core" :refer [Box Autocomplete TextField]]
   [reframe-blog.router :as router]
   [reframe-blog.nav.subs :as subs]
   [reframe-blog.nav.events :as events]
   [reframe-blog.components.nav-search-field :refer [search-field]]
   [reframe-blog.nav.views.nav-item :refer [nav-item]]))

(defn public [box-props]
  (let [active-page @(rf/subscribe [::subs/active-page])
        nav-search-value (rf/subscribe [::subs/nav-search-value])
        nav-items
        [{:id :posts
          :name "Posts"
          :href (router/path-for :posts)
          :dispatch #(do
                      (rf/dispatch [::events/set-active-page :posts])
                      (rf/dispatch [::events/set-visibility false]))
          :active-page active-page}
         {:id :about
          :name "About"
          :href (router/path-for :about)
          :dispatch #(do
                      (rf/dispatch [::events/set-active-page :about])
                      (rf/dispatch [::events/set-visibility false]))
          :active-page active-page}]]

    [:> Box box-props
     
     (for [item nav-items]
       ^{:key item}

       [nav-item item])]))
