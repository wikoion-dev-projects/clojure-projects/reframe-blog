(ns reframe-blog.home.subs
  (:require
   [re-frame.core :as rf]
   ["moment" :as moment]))

(rf/reg-sub
 ::latest-3-posts
 (fn [db _]
   (take 3 (sort
            (fn [x y]
              (.
               (moment (js/parseInt (:date x))) isAfter
               (moment (js/parseInt (:date y)))))
            (:posts db)))))
