(ns reframe-blog.home.views.home
  (:require
   [reagent.core :as r]
   [re-frame.core :as rf]
   ["@material-ui/core" :refer [Box]]
   [reframe-blog.home.events :as events]
   [reframe-blog.post.views.post-card :refer [post-cards]]
   [reframe-blog.home.subs :as subs]))

(defn home-page []
  [:> Box
   [:> Box
    {:id "home-image"
     :box-shadow 2
     :style {:width "100%"
             :height "650px"
             :overflow "hidden"
             :background-image (str "linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.35) ), url(" "https://gitlab.com/wikoion-dev-projects/draycott-live-blog/-/raw/master/src/main/resources/static/images/home-bg.jpg" ")")
             :background-size "cover"
             :display "flex"
             :justify-content "center"
             :align-items "center"
             }}

    [:> Box
     {:id "home"
      :text-align "center"}
     [:h1.home "CALL ME JOHNSON"]
     [:h3.home "\"CONTAINS MUCH THAT IS APOCRYPHAL, OR AT LEAST WILDLY INACCURATE\""]]]

   [post-cards (rf/subscribe [::subs/latest-3-posts])]])
