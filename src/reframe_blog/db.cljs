(ns reframe-blog.db
  (:require))

(def default-db
  {:nav {:active-nav :home
         :active-page :home
         :active-post nil}

   :loading? false

   :search-loading? false

   :error nil

   :success-http-result nil

   :failure-http-result nil

   :visibility false

   :search-popover-visibility false

   :search-popover-anchor nil

   :window-size (.-innerWidth js/window)

   :nav-search-value ""

   :failure-search-result nil

   :search-results []
   
   :posts []

   :highlighted-projects [{:id :project-01
                          :name "Re-frame Blog"
                          :image "/img/reframe.png"
                          :href "https://gitlab.com/wikoion-dev-projects/clojure-projects/reframe-blog"
                          :description "This site! An SPA written in clojurescript, using Reagent and Re-frame"}
                         {:id :project-02
                          :name "Go Blog API"
                          :image "/img/go.png"
                          :href "https://gitlab.com/wikoion-dev-projects/go-projects/golang-blog"
                          :description "A blog API written in Go, with MongoDB backend, the API used by this site!"}
                         {:id :project-03
                          :name "Self Hosted"
                          :image "/img/traefik.png"
                          :href "https://gitlab.com/self-hosted-container-services"
                          :description "Docker stacks providing example configuration for various containerised services"}
                         {:id :project-04
                          :name "Ansible Arch installer"
                          :image "/img/ansible.jpg"
                          :href "https://gitlab.com/system-configuration-files/arch-zfs-ansible-install"
                          :description "Uses Ansible to install Arch on an encrypted ZFS filesystem"}
                         {:id :project-05
                          :name "Archiso Builder"
                          :image "/img/arch.jpg"
                          :href "https://gitlab.com/system-configuration-files/archiso-zfs"
                          :description "A docker container that builds an archiso with Ansible Arch Installer and deps."}
                         {:id :project-06
                          :name "Dotfiles"
                          :image "/img/dotfiles.png"
                          :href "https://gitlab.com/system-configuration-files/dotfiles"
                          :description "Configuration files for Nord themed i3WM, tmux, vim and polybar"}
                          {:id :project-07
                          :name "Java Blog"
                          :image "/img/draycott-live-blog.gif"
                          :href "https://gitlab.com/wikoion-dev-projects/draycott-live-blog"
                          :description "An MPWA, blogging platform. Written using Java, Kotlin and Spring. *WIP*"}
                         ]})
