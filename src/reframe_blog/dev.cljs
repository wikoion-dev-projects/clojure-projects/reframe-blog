(ns reframe-blog.dev)

(goog-define dev false)

(defn api-url []
  (if (false? dev)
    "https://api.fossg.news/posts"
    "http://localhost:8080/posts"))
