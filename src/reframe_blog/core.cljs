(ns reframe-blog.core
  (:require
   [reagent.core :as r]
   [reagent.dom :as rd]
   [re-frame.core :as rf]
   [reframe-blog.events :as events]
   [reframe-blog.router :as router]
   [reframe-blog.views :as views]
   [reframe-blog.config :as config]
   ["@material-ui/core" :refer [Button]]
   ;; ---nav ---
   [reframe-blog.nav.events]
   [reframe-blog.nav.subs]
   ))

(defn dev-setup []
  (when config/debug?
    (println "dev mode")))

(defn ^:dev/after-load mount-root []
  (rf/clear-subscription-cache!)
  (rd/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn init []
  (router/start!)
  (rf/dispatch-sync [::events/initialize-db])
  (dev-setup)
  (mount-root))
