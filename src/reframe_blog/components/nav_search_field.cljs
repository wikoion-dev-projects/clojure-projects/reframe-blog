(ns reframe-blog.components.nav-search-field
  (:require
   [reframe-blog.nav.subs :as subs]
   [reframe-blog.post.subs :as post-subs]
   [reframe-blog.nav.events :as events]
   [reframe-blog.router :as router]
   [reagent.core :as r]
   [re-frame.core :as rf]
   ["@material-ui/core" :refer [InputAdornment IconButton Box Grid TextField Paper Button ClickAwayListener Popper]]
   ["@material-ui/lab" :refer [Autocomplete]]
   ["@material-ui/icons" :refer [Clear Search]]))

(def anchor-el (r/atom ""))
(def adornment-vis (r/atom false))

(defn clear-search []
  (rf/dispatch [::events/set-nav-search-value ""])
  (swap! adornment-vis (fn [x] false)))

(defn event-value
  [^js/Event e]
  (let [^js/HTMLInputElement el (.-target e)]
    (.-value el)))

(def input-component
  (r/reactify-component
   (fn [props]
     [:input (-> props
                 (assoc :className (str (:className props) " search-input"))
                 (assoc :ref (:inputRef props))
                 (dissoc :inputRef))])))

(defn empty-search? [e]
  (or
   (= (event-value e) "")
   (nil? (event-value e))))

(defn set-popover-visibility [bool]
  (rf/dispatch [::events/set-search-popover-visibility bool]))

(defn set-search-value [query]
  (rf/dispatch [::events/set-nav-search-value query]))

(defn popover-click-handler [e]
  (let [popover-visibility @(rf/subscribe [::subs/search-popover-visibility])]
    (swap! anchor-el #(.-target e))
    (if (not (empty-search? e))
      (if (= popover-visibility false)
        (set-popover-visibility true))
     (set-popover-visibility false))))

(defn popover-change-handler [e]
  (let [popover-visibility @(rf/subscribe [::subs/search-popover-visibility])]
    (set-search-value (event-value e))
    (swap! anchor-el #(.-target e))
    (if (not (empty-search? e))
      (do
        (swap! adornment-vis (fn [x] true))
        (if (= popover-visibility false)
          (set-popover-visibility true)))
      (do
        (set-popover-visibility false)
        (swap! adornment-vis (fn [x] false))))))

(defn search-popover-content [e]
  (let [posts (rf/subscribe [::post-subs/search-results])]
    (if (and (> (count @posts) 0) (not (empty? (get @posts 0))))
      [:> Box
       {:style {:padding-bottom "10px"}}

       (for [post @posts]
         ^{:key post}
         [:> Grid
          {:container true
           :direction "row"
           :class "search-popover-content"
           :spacing 2
           :on-click #(do
                        (router/set-token! (router/path-for :post :post-id (:id post)))
                        (rf/dispatch [::events/set-visibility false]))
           :style {:padding-right "15px"
                   :padding-left "15px"
                   :max-height "150px"}}

          [:> Grid
           {:item "true"}
           [:img
            {:src (:image post)
             :style {:width "100px"
                     :height "100px"}}]]
       
          [:> Grid
           {:item "true"
            :style {:margin-top "-10px"}}
           [:h3
            {:style {:margin-bottom "-5px"}}
            (let [title (apply str (take 28 (:title post)))]
              (if (> (count (:title post)) 28)
                (str title "...")
                title))]
           [:p (str (apply str (take 35 (:body post))) "...")]]])])))

(defn search-adornment []
  (r/as-element
   [:> InputAdornment
    {:class "search-adornment"
     :position "end"}
     (r/as-element
      [:> IconButton
       {:on-click #(clear-search)
        :style {:margin-right "-15px"}}
       (r/as-element [:> Clear])])]))

(defn search-field []
  (let [field (r/adapt-react-class TextField)
        nav-search-value (rf/subscribe [::subs/nav-search-value])
        set-anchor-el (rf/dispatch [::events/set-search-popover-anchor])]
    [field
     {:variant "outlined"
      :class "search-container"
      :size "small"
      :placeholder "Search"
      :default-value ""
      :value @nav-search-value
      :on-change #(popover-change-handler %)
      :on-click #(popover-click-handler %)
      :InputProps {:inputComponent input-component
                   :endAdornment (if (true? @adornment-vis)
                                   (search-adornment))
                   :classes {:adornedEnd "search-input-adorned"}}
      :style {:padding-top "10px"}}]))

(defn search-component []
  (let [popover-open (rf/subscribe [::subs/search-popover-visibility])]
    [:> Box
     {:display "flex"}
     [search-field]
     [:> ClickAwayListener
      {:on-click-away #(rf/dispatch [::events/set-search-popover-visibility false])}

      [:> Popper
       {:class "search-popover"
        :open @popover-open
        :anchor-el @anchor-el
        :placement "bottom-start"}
       [search-popover-content]]]]))

(defn auto-complete []
  [:> Autocomplete
   {:options (mapv :title @(rf/subscribe [::subs/search-results]))
    :style {:padding-top "10px"
            :width "100%"}
    :render-input (fn [^js params]
                    (set! (.-variant params) "outlined")
                    (set! (.-size params) "small")
                    (set! (.-placeholder params) "Search")
                    (set! (.-value params) @(rf/subscribe [::subs/nav-search-value]))
                    (set! (.-onChange params) #(rf/dispatch [::events/set-nav-search-value (event-value %)]))
                    (r/create-element TextField params))}])
