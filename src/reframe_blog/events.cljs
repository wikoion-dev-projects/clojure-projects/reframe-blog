(ns reframe-blog.events
  (:require
   [re-frame.core :as rf]
   [reframe-blog.db :as db]
   [ajax.core :as ajax]
   [reframe-blog.dev :refer [api-url]]
   [day8.re-frame.http-fx]))

(rf/reg-event-db
 ::initialize-db
 (fn [_ _]
   db/default-db))

(defn lowercase-keywordise [coll]
  (apply vector
         (for [m coll]
           (reduce merge
                   (map (fn [x]
                          {(keyword (clojure.string/lower-case (name (key x))))
                           (val x)})
                        m)))))

(defn keywordise-id [coll]
  (mapv
   (fn [x]
     (assoc x :id (keyword (:id x))))
   coll))

(defn upper-case-title [coll]
  (mapv
   (fn [x]
     (assoc x :title (clojure.string/upper-case (:title x))))
   coll))

(rf/reg-event-fx                             
  ::get-posts                      
  (fn [{:keys [db]} _]                    
    {:db   (assoc db :loading? true)   
     :http-xhrio {:method          :get
                  :uri             (api-url)
                  :timeout         8000                                           
                  :format          (ajax/json-request-format)
                  ;; :headers         {"Authorization"
                  ;;                   (str "bearer " "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VybmFtZSI6ImFkbWluIn0.X5qKoep6z071prwVB4EKUhGvNac18M9SZvpLKgVQaiY")}
                  :response-format (ajax/json-response-format {:keywords? true})  
                  :on-success      [::good-posts-result]
                  :on-failure      [::bad-http-result]}}))

(rf/reg-event-db
 ::good-posts-result
 (fn [db [_ result]]
   (-> db
       (assoc-in [:loading?] false)
       (assoc-in [:posts]
                 (-> result
                     (lowercase-keywordise)
                     (keywordise-id)
                     (upper-case-title))))))

(rf/reg-event-db
 ::bad-http-result
 (fn [db [_ result]]
   (assoc-in db [:loading?] false)
   (assoc db :failure-http-result result)))
