(ns reframe-blog.router
  (:require
   [bidi.bidi :as bidi]
   [pushy.core :as pushy]
   [re-frame.core :as rf]
   ))

(def routes ["/" {"" :home
                  "about"  :about
                  "about/"  :about
                  "create" :create
                  "create/" :create
                  "post" {"" :posts}
                  "post/" {"" :posts
                           [:post-id] :post}
                  "posts" {"" :posts}
                  "posts/" {"" :posts
                            [:search-query] :search}}])

(def history
  (let [dispatch #(rf/dispatch [:route-changed %])
        match #(bidi/match-route routes %)]
    (pushy/pushy dispatch match)))


(defn start! []
  (pushy/start! history))

(def path-for
  (partial bidi/path-for routes))

(defn match-route [route]
  (bidi/match-route routes route))

(defn set-token! [token]
  (pushy/set-token! history token))
