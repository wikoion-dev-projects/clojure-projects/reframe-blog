(ns reframe-blog.footer.views.footer
  (:require
   ["@material-ui/core" :refer [Box Button]]))

(defn footer []
  [:footer
   [:> Box
    {:class "footer"}
    [:h3 "Quick Links"]

    [:> Box
     {:style {:margin-top -20}}
     [:> Button
      {:href "https://gitlab.com/wikoion"}
      [:img
       {:src "/img/gitlab-icon.svg"
        :style {:width 70
                :height 70
                :filter "invert(77%) sepia(33%) saturate(4085%) hue-rotate(209deg) brightness(88%) contrast(83%)"}}]]

     [:> Button
      {:href "https://github.com/wikoion"}
      [:img
       {:src "/img/github-icon.png"
        :style {:width 40
                :height 40
                :filter "invert(77%) sepia(33%) saturate(4085%) hue-rotate(209deg) brightness(88%) contrast(83%)"
                :padding-right 10}}]]

     [:> Button
      {:href "https://twitter.com/callmejonnson"}
      [:img
       {:src "/img/twitter-icon.svg"
        :style {:width 40
                :height 40
                :filter "invert(77%) sepia(33%) saturate(4085%) hue-rotate(209deg) brightness(88%) contrast(83%)"}}]]]]]
  )
  
